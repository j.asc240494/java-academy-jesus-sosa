package com.java.academy.javaacademyjesussosa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaAcademyJesusSosaApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaAcademyJesusSosaApplication.class, args);
	}

}
